

install : 
	# you may want to sudo make install
	apt-get install nodejs npm
	npm install socket.io


run:
	nodejs chat.js > stdout.txt & 
	# launch the "nodejs chat.js" and write the console output in  stdout.txt 
	#&: run in background / release the console (when you close your ssh client the process do not stop)
	
	