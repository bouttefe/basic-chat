

/*
Copyright 2016 Quentin Bouttefeux


This file is part of Basic chat.

    Basic chat is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    Basic chat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Basic chat.  If not, see <http://www.gnu.org/licenses/>.

*/


/***********************************************************
 *                                                         *
 *                        Code option                      *
 *                                                         *
 ***********************************************************/

'use strict'; //  same as JS strict option

/***********************************************************
 *                                                         *
 *                   importing nodeJS module               *
 *                                                         *
 ***********************************************************/

var http = require('http'); // for the HTTP server
var url = require("url");  // for getting url from a http request
var querystring = require('querystring'); 

var os = require('os'); 
var fs = require("fs"); // file syst�me for write read in file


/***********************************************************
 *                                                         *
 *                   Variable decalration                  *
 *                                                         *
 ***********************************************************/

var dataChat = []; // empty data set
// we will read these data from a file "/data/messages.json"

var messagesFileName  = "./data/messages.json"; // the path to the file where the message are stored

var chatHTMLpage = "./html-page/chat-page.html" // the HTML page of the chat

/***********************************************************
 *                                                         *
 *                      Classes constructor                *
 *                                                         *
 ***********************************************************/

function Message(message,name,id) {
	/*
	 *
	 * constructor of the Message Object
	 *
	 */ 
	var messageTemp = message
	
	messageTemp = message.replace(/<\/?[^>]+(>|$)/g, ""); // remove html tag from a string
	
	this.message = messageTemp; // the body of the message
	this.senderName = name.replace(/<\/?[^>]+(>|$)/g, ""); // the name of the sender
	this.id = id; // id for the message
	
	
}

/***********************************************************
 *                                                         *
 *                     useful function                     *
 *                                                         *
 ***********************************************************/

function writeData(){
	/*
	 * wtite the messages data in the file data file
	 *
	 */
	fs.writeFile(messagesFileName,JSON.stringify(dataChat),function (err) {}); // write the message in the file
}

function getHTLMView(arrayMessage){
	//               ^^^ an array of Mesage object
	/*
	 * return th thml code to show messages stored in arrayMessage
	 */
	var messageToSend = "";
	
	for (var i = arrayMessage.length-1; i>=0;--i){ // read the message from last to the the first
		// old style
		/*messageToSend+= "<div class='chat-div'>";
		messageToSend+= "<div class=innerBox>";
		messageToSend+= "<strong class=nameBox>"+arrayMessage[i].senderName+"</strong> <label class=timeLabel></label><br>";
		messageToSend+= "<label class='labelText'>"+arrayMessage[i].message+"</label>";
		messageToSend+= "</div> </div>";*/
		
		
		// new style
		
		messageToSend+="<div class='messages'>";
		messageToSend+='<div class="message">';
		//messageToSend+='<div class="triangle"></div>';
		//messageToSend+='<img src="" class="avatar">';
		messageToSend+='<p><strong>'+arrayMessage[i].senderName+',<em> '+'</em></strong>';
		messageToSend+=arrayMessage[i].message+'</p></div></div>';
	    
	}
	
	return messageToSend;
}
function getHTLMViewPerso(arrayMessage){
	//                   ^^^ an array of Mesage object
	/*
	 * return th thml code to show messages stored in arrayMessage
	 * this message has a different look than getHTLMView
	 * to show that it's the viewer that send it
	 */
	var messageToSend = "";
	
	for (var i = arrayMessage.length-1; i>=0;--i){ // read the message from last to the the first
		
		// old style
		/*messageToSend+= "<div class='chat-perso'>";
		messageToSend+= "<div class=innerBox>";
		messageToSend+= "<strong class=nameBox>"+arrayMessage[i].senderName+"</strong> <label class=timeLabel></label><br>";
		messageToSend+= "<label class='labelText'>"+arrayMessage[i].message+"</label>";
		messageToSend+= "</div> </div>";*/
		
		
		
		//new style
		messageToSend+="<div class='messages'>"
		messageToSend+='<div class="message inverse">'
		//messageToSend+='<div class="triangle"></div>'
		//messageToSend+='<img src="" class="avatar">'
		messageToSend+='<p><strong>'+arrayMessage[i].senderName+',<em> '+'</em></strong>'
		messageToSend+=arrayMessage[i].message+'</p></div></div>';
	    
	}
	
	return messageToSend;
}



function getNewId(){
	/*
	 * get a new ID for message in dataChat
	 */
	if (dataChat.length>0) {
		return dataChat[dataChat.length-1].id+1; // take the id of the last element of dataChat and add one
	}
	else{
		return 0;
	}
	
}

function addNewMessage(newMessage){
	/*
	 *
	 * try to add the message to the data
	 *
	 * if sucessful the return true
	 * else return false
	 * 
	 */
	
	if (newMessage.message != undefined && newMessage.name != undefined) { // if the field of newMessage is valid
		
		var newId = getNewId(); // take the id of the last element of dataChat and add one
		var messageToAdd = new Message(newMessage.message,newMessage.name,newId)
		dataChat.push(messageToAdd); // add this message to the data
		writeData();
		return true;
	}
	else{
		return false;
	}
}

/***********************************************************
 *                                                         *
 *                      read data file                     *
 *                                                         *
 ***********************************************************/




fs.readFile(messagesFileName,'utf8', function (err, data) {
	// this fuction will prosses asychronously when the file is read (the function doesn't trigger directly when the fs.readFile is called)
	// the data is the string read and err is undefined if there is no error but if there is an error err then err is the error message
	if ( ! err) {
		// if there is no error
		
		if (data == "" ) { // it the file is empty
			
			dataChat =[]; // empty array
		}
		else{
			
			try{
				dataChat = JSON.parse(data); 
			}
			catch(e){
				console.log("[error] faile to JSON parse the data in " +messagesFileName +" : "+e); // log some string in the console
				
				
				process.exit(1);// stop the programme
				// 1 to signal that there is an error
				// if there is no error call it with the argument 0
				
			}
			finally{
				
			}
		}
	}
	else{
		console.log("[error] can not read " +messagesFileName +" : "+err); // log some string in the console
		
		
		process.exit(1);// stop the programme
		// 1 to signal that there is an error
		// if there is no error call it with the argument 0
	}
	
})


/***********************************************************
 *                                                         *
 *                      create server                      *
 *                                                         *
 ***********************************************************/



// creat a http server
// this function will trigger each time teh server recice a http request
// the request will be passed in the "request" variable and the response send by this server is send througth response
var server = http.createServer(function(request,response){
	
	var page = url.parse(request.url).pathname; // set page a the page of the request
	// by instance
	// GET on http://name/page/subpage/file
	// page will be at /page/subpage/file
	
	// with response I mostly user response.writeHead(#code, #text, {'Content-Type': #type});
	// where #code is the HTTP code of the reponse (see en: <https://en.wikipedia.org/wiki/List_of_HTTP_status_codes> or fr : <https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP>)
	// #text is the string related to the HTTP code (I generaly use the standart one)
	// and #type is the type (MIME) of the message sended . Can be
	// -'text/plain' : basic texte
	// -'text/html' : html
	// -'application/javascript' : for JS code
	// -'application/json' : for strigify JSON data
	//
	//
	// response.end(#string);  write #string ine the reponse and  end a reponse
	// note : a response must always be ended
	//
	//response.write(#string); write #string in the response
	
	if (request.method == 'POST') { // if the request method is "POST"
		
		/*
		 * In the poste methode for the chat we will recive the chat message we wille be storing it and sendig it back
		 */ 
		
		var body = ''; // the body (data) of the request 
		request.setEncoding('utf8'); // set the encding in utf-8
		
		request.on('data', function (data) {
			body += data;
			// Too much POST data, kill the connection!
			// 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
		    
			if (body.length > 1e6){ // if there is too much data
				request.connection.destroy();  // destroy the connenction
				response.writeHead(413, 'Request Entity Too Large', {'Content-Type': 'text/plain'});
				response.end('');
			}
			else{
				
			}
		    
		});
		var post
		request.on('end', function () { // � la fin
			
			post = querystring.parse(body);
			// the data are stored (string) in body
			// we parse with JSON
			
			if (page == "/messages") {
				try{
					var newMessage = JSON.parse(body);
					var messageAdded = addNewMessage(newMessage)
					if (messageAdded) { 
						
						response.writeHead(200, 'OK', {'Content-Type': 'text/plain'});
						response.end('');
					}
					else{
						response.writeHead(400, 'Bad Request', {'Content-Type': 'text/plain'}); 
						response.end('data illformed');
					}
				}
				catch(e){
					// mostlikely if the body can not be parsed
					response.writeHead(400, 'Bad Request', {'Content-Type': 'text/plain'}); 
					response.end('data illformed : error while parsing data');
				}
				finally{
					
				}
				
				
			}
			else{
				response.writeHead(404, 'Page not found', {'Content-Type': 'text/plain'});
				response.end('Page not found');
			}
		});
	}
	else if(request.method == 'GET'){
		/*
		 * in the get methode we will send back the chat page and the message sended
		 *
		 */ 
		if (page =="/") {
			response.writeHead(200, 'OK', {'Content-Type': 'text/plain'});
			fs.readFile(chatHTMLpage,'utf8', function (err, data) {
				if (err) {
					response.writeHead(404, 'Page not found', {'Content-Type': 'text/plain'});
					response.end('Page not found');
				}
				else{
					response.writeHead(200, 'OK', {'Content-Type': 'text/html'});
					response.end(data);
				}
			});
	}
		else if (page =="/messages") {
			response.writeHead(200, 'OK', {'Content-Type': 'application/json'});
			response.end(JSON.stringify(dataChat)); // send the data
		}
		else{
			response.writeHead(404, 'Page not found', {'Content-Type': 'text/plain'});
			response.end('Page not found');
		}
	}
	else{
		response.writeHead(400, {'Content-Type': 'text/plain'}); // will send the error 400
		response.end("Bad Request")
	}
});


/***********************************************************
 *                                                         *
 *                      socket.io                          *
 *                                                         *
 ***********************************************************/

/*
 * if you want to use socket.io don't forget to do "npm install socket.io" 
 *
 */

var io = require('socket.io').listen(server);// ceration of the socket

io.listen(server); // binding the socket with the server


io.sockets.on('connection', function (socket) { // when the client connect to the socket
	
	//          type of message      message : can be an object
	socket.emit('allMessage', getHTLMView(dataChat) );
	
	socket.on('sendMessage', function (newMessage) { // when the server recive a message with sendMessage type
		
		
		
		var messageAdded = addNewMessage(newMessage); // try to add the message 
		if (messageAdded) {
			// get the html view of the last message (a array with only one message)
			var arrayNewMessage = [dataChat[dataChat.length-1]];
			var messageToBroadcast = getHTLMView(arrayNewMessage);
			
			socket.broadcast.emit('newMessage', messageToBroadcast); // send to all client execpt this one
			socket.emit('newMessage', getHTLMViewPerso(arrayNewMessage) );  // send the message to this client
		}
		else{
			
		}
		
	}); 
});


/***********************************************************/

server.listen(8080); // say to witch port the server is accepting request (HTTP)


/***********************************************************
 *                                                         *
 *                          End                            *
 *                                                         *
 ***********************************************************/
